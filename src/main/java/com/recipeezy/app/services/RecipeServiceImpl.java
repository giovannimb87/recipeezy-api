package com.recipeezy.app.services;


import com.recipeezy.app.entity.Recipe;
import com.recipeezy.app.repository.RecipeRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Slf4j
public class RecipeServiceImpl implements RecipeService {

    @Autowired
    RecipeRepository recipeRepository;

    @Override
    public Iterable<Recipe> getAllRecipes() {
        return recipeRepository.findAll();
    }

    @Override
    public void addRecipe(Recipe recipe) {
        recipeRepository.save(recipe);
    }

    @Override
    public Recipe findRecipeById(int id) {
        Optional<Recipe> opt = recipeRepository.findById(id);
        return opt.orElse(null);
    }

    @Override
    public HttpStatus deleteRecipe(int id) {
        Optional<Recipe> opt = recipeRepository.findById(id);
        if (opt.isPresent()) {
            recipeRepository.deleteById(id);
            return HttpStatus.OK;
        }
        return HttpStatus.BAD_REQUEST;
    }


    @Override
    public HttpStatus updateRecipe(Recipe recipe) {
            recipeRepository.save(recipe);
            return null;
        }
}
