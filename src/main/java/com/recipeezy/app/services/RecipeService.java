package com.recipeezy.app.services;

import com.recipeezy.app.entity.Recipe;
import org.springframework.http.HttpStatus;

public interface RecipeService {

    Iterable<Recipe> getAllRecipes();
    void addRecipe(Recipe recipe);
    Recipe findRecipeById(int id);
    HttpStatus deleteRecipe(int id);
    HttpStatus updateRecipe(Recipe recipe);

}
