package com.recipeezy.app.controller;

import com.recipeezy.app.entity.Recipe;
import com.recipeezy.app.services.RecipeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping(value = "/recipes")
public class RecipeController {

    @Autowired
    RecipeService recipeService;

    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping("")
    public ResponseEntity<Iterable<Recipe>> retrieveAllRecipes() {
        return new ResponseEntity<>(recipeService.getAllRecipes(), HttpStatus.OK);
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PostMapping("/add")
    public ResponseEntity<HttpStatus> postRecipeHandler (@RequestBody Recipe recipe) {
        recipeService.addRecipe(recipe);
        return ResponseEntity.ok(HttpStatus.CREATED);
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PutMapping(path = "edit/{id}")
    public ResponseEntity<HttpStatus> updateRecipe(@RequestBody Recipe recipe, int id){
        HttpStatus status = recipeService.updateRecipe(recipe);
        return ResponseEntity.ok(status);
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @DeleteMapping("/remove/{id}")
    public ResponseEntity<HttpStatus> removeRecipeHandler (@PathVariable int id) {
        Optional<Recipe> opt = Optional.ofNullable(recipeService.findRecipeById(id));
        if(opt.isPresent()){
            recipeService.deleteRecipe(id);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

}
