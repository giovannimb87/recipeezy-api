package com.recipeezy.app.controller;

import com.recipeezy.app.entity.Recipe;
import com.recipeezy.app.repository.RecipeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping(path = "")
public class HomeController {

    @GetMapping("")
    public ResponseEntity<String> renderHomePage() {
        return new ResponseEntity<>("Hello", HttpStatus.OK);
    }

}
