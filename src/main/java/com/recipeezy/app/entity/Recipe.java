package com.recipeezy.app.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name="recipe")
public class Recipe {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    Integer id;

    @Column(name = "name")
    String name;

    @Column(name = "description")
    @Size(min = 1, max = 5000)
    String description;

    @Column(name = "ingredients")
    @Size(min = 1, max = 5000)
    String ingredients;

    @Column(name = "directions")
    @Size(min = 1, max = 20000)
    String directions;

    @Column(name = "image")
    @Size(max = 500)
    String image;

}
